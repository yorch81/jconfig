# JConfig #

## Description ##
Manage JSON Configuration Files. 

## Requirements ##
* [.NET Framework](http://www.microsoft.com/es-mx/download/details.aspx?id=30653)
* [Mono](http://www.mono-project.com/)
* [Json.NET](http://www.newtonsoft.com/json)

## Developer Documentation ##
In the Code.

## Installation ##
Add nuget reference: 
	PM> Install-Package JConfig.dll

## Example ##
~~~

JConfig.JConfig cfg = new JConfig.JConfig("config.json");

cfg.setKey("serverDb", serverDb);

cfg.save();

~~~

## References ##
http://www.json.org/




