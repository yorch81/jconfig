﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// JConfig 
//
// JConfig JSON configuration file Manager
//
// Copyright 2016 Jorge Alberto Ponce Turrubiates
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace JConfig
{
    /// <summary>
    /// Manage JSON configuration file
    /// </summary>
    public class JConfig
    {
        /// <summary>
        /// JSON Object
        /// </summary>
        JObject _json = null;

        /// <summary>
        /// File Name
        /// </summary>
        string _file = "";

        /// <summary>
        /// Constructor of class
        /// </summary>
        /// <param name="file">
        /// File Name
        /// </param>
        public JConfig(string file)
        {
            _json = new JObject();
            _file = file;

            loadFile();
        }

        /// <summary>
        /// Get total number of Keys
        /// </summary>
        /// <returns>
        /// int
        /// </returns>
        public int totalKeys()
        {
            return _json.Count;
        }

        /// <summary>
        /// Set Key Item
        /// </summary>
        /// <param name="key">
        /// Key Name
        /// </param>
        /// <param name="value">
        /// Value of Key
        /// </param>
        public void setKey(string key, string value)
        {
            JToken result;

            _json.TryGetValue(key, out result);

            if (result == null)
                _json.Add(key, value);
            else
            {
                _json.Remove(key);
                _json.Add(key, value);
            }
        }

        /// <summary>
        /// Get Value of Key
        /// </summary>
        /// <param name="key">
        /// Key Name
        /// </param>
        /// <returns>
        /// string
        /// </returns>
        public string getValue(string key)
        {
            JToken result;

            _json.TryGetValue(key, out result);

            if (result == null)
                return string.Empty;
            else
                return result.ToString();
        }

        /// <summary>
        /// Save JSON File
        /// </summary>
        public void save()
        {
            System.IO.File.WriteAllText(_file, _json.ToString());
        }

        /// <summary>
        /// Load JSON File
        /// </summary>
        private void loadFile()
        {
            if (System.IO.File.Exists(_file))
            {
                string content = System.IO.File.ReadAllText(_file);

                try
                {
                    _json = JObject.Parse(content);
                }
                catch (JsonReaderException e)
                {
                    _json = new JObject();
                }
            }
        }
    }
}
